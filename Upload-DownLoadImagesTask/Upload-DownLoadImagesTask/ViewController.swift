//
//  ViewController.swift
//  Upload-DownLoadImagesTask
//
//  Created by Click Labs on 3/23/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //image view use to show selected images...
    @IBOutlet weak var myImageView: UIImageView!
    
    //button code to download images with given url...
    @IBAction func downloadImages(sender: AnyObject) {
            //specified url from where image downloaded...
            var fileURL: NSString = "http://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Leonardo_da_Vinci_-_Adorazione_dei_Magi_-_Google_Art_Project.jpg/250px-Leonardo_da_Vinci_-_Adorazione_dei_Magi_-_Google_Art_Project.jpg"
        
            //call function, responsible for downloading images..
            getImageFromURL(fileURL)
    }
    
    //button code to upload images with given url...
    @IBAction func uploadButton(sender: AnyObject) {
            myImageUploadRequest() //call function, responsible for uploading images..
    }
    
    //button code to select images from photo galary...
    @IBAction func selectImageButton(sender: AnyObject){
            var mySelectedImagesFrom  = UIImagePickerController()
            mySelectedImagesFrom.delegate = self
            mySelectedImagesFrom.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.presentViewController(mySelectedImagesFrom, animated: true, completion: nil)
    }
    //code to show selected image in imageview box...
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
            myImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //function which is responsible for uploading images..
    func myImageUploadRequest()
    {
            let myUrl = NSURL(string: "https://abdulshamimkhan5.wordpress.com/");
            let request = NSMutableURLRequest(URL:myUrl!);
            request.HTTPMethod = "POST";
        
            let param = [
                "firstName" : "Sergey",
                "lastName" : "Kargopolov",
                "userId" : "9"
            ]
        
        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        let imageData = UIImageJPEGRepresentation(myImageView.image, 1)
        if(imageData==nil) { return; }
        request.HTTPBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData, boundary: boundary)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                data, response, error in
            
                if error != nil {
                    println("error=\(error)")
                    return
                }
                // You can print out response object
                println("******* response = \(response)")
                // Print out reponse body
                let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("****** response data = \(responseString!)")
                var err : NSError?
                var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: &err) as? NSDictionary
            
                dispatch_async(dispatch_get_main_queue(),{
                    // self.myActivityIndicator.stopAnimating()
                    self.myImageView.image = nil;
                });
            }
            task.resume()
    }
    
    //function which is responsible for downloading images..
    func getImageFromURL(fileURL : NSString)
    {
            var url: NSURL!
            url = NSURL(string: fileURL)
            var err: NSError?
            var imageData :NSData = NSData(contentsOfURL: url, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)!
            var bgImage = UIImage(data:imageData)
            let pathsArray = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
            let documentsDirectory = pathsArray[0] as String
            println(documentsDirectory)
            let savePath = documentsDirectory.stringByAppendingPathComponent("image_FileName.jpg")
            UIImagePNGRepresentation(bgImage).writeToFile(savePath, atomically: true)
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        
            var body = NSMutableData();
        
            if parameters != nil {
                for (key, value) in parameters! {
                body.appendString("–\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
                }
            }
        
            let filename = "user-profile.jpg"
            let mimetype = "image/jpg"
            body.appendString("–\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimetype)\r\n\r\n")
            body.appendData(imageDataKey)
            body.appendString("\r\n")
            body.appendString("–\(boundary)–\r\n")
            return body
        }
    
        func generateBoundaryString() -> String {
            return "Boundary-\(NSUUID().UUIDString)"
        }
    }
        extension NSMutableData {
            func appendString(string: String) {
                let data = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
                appendData(data!)
            }
        
    }

